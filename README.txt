CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

Current Maintainer: Chris Hoffman <https://www.drupal.org/u/hoff331>

The Real Name per Role module extends the Real Name module <https://www.drupal.org/project/realname> by providing a UI that allows admins to set the Real Name per Drupal role rather than site wide.


INSTALLATION
------------

This should outline a step-by-step explanation of how to install the
module.

1. Install as usual, see http://drupal.org/node/70151 for further information.

2. Visit the realname per role config page at admin/config/people/realname/realname_per_role

3. Set the realname pattern per role and save.