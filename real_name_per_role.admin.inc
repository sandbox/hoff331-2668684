<?php

/**
 * Form constructor for the Realname per role settings form.
 *
 * @see realname_per_role_settings_form_validate()
 * @see realname_per_role_settings_form_submit()
 */
function realname_per_role_settings_form($form, &$form_state) {
  //load all Drupal roles
  $user_roles = user_roles(FALSE, NULL);
  //loop through roles
  foreach ($user_roles as $user_role) {
    //remove the space and word 'user' from user role and assign value to role
    $role = str_replace(" user", "", $user_role);
    //provide form textfield per role
    $form['realname_per_role_' . $role] = array(
      '#type' => 'textfield',
      '#title' => t('@role role: real name pattern', array('@role' => $role)),
      '#default_value' => variable_get('realname_per_role_' . $role, '[user:name-raw]'),
      '#element_validate' => array('token_element_validate'),
      '#token_types' => array('user'),
      '#min_tokens' => 1,
      '#required' => TRUE,
      '#maxlength' => 256,
    );
  }

  //TO DO each role's form should have token helper
  $form['token_help'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array('user'),
    '#global_types' => FALSE,
    '#dialog' => TRUE,
  );

  //form submit and form handler
  $form['#submit'][] = 'realname_per_role_settings_form_submit';
  return system_settings_form($form);
}

/**
 * Form validation handler for realname_per_role_settings_form().
 *
 * @see realname_per_role_settings_form_submit()
 */
function realname_per_role_settings_form_validate($form, &$form_state) {
  $user_roles = user_roles(FALSE, NULL);
  $flag = FALSE;
  //loop through roles
  foreach ($user_roles as $user_role) {
    //remove the space and word 'user' from user role and assign value to role
    $role = str_replace(" user", "", $user_role);
    //if [user:name] token exists in any pattern field, set flag to true
    if (strpos($form_state['values']['realname_per_role_' . $role], '[user:name]') !== FALSE) {
      $flag = TRUE;
    }
  }
  //if flag true, throw error
  if($flag){
    form_set_error('realname_pattern', t('The <em>[user:name]</em> token cannot be used as it will cause recursion.'));
  }
}

/**
 * Form submission handler for realname_per_role_settings_form().
 *
 * @see realname_per_role_settings_form_validate()
 */
function realname_per_role_settings_form_submit($form, $form_state) {
  // Clear the realname cache
  realname_delete_all();
}
